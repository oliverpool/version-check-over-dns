# Release Version Check Over DNS (RVCoDNS)

Quickly check if the currently running version of a software is up-to-date.

## What and Why

Usually, checking if the currently running version of a software is up-to-date is achieved by getting a `version.json` file over HTTP(S) and comparing its value. Performing such a check over HTTP(S) is not perfect:

- it discloses the IP of the system running the software (could lead to privacy issue)
- it is hard to scale (requires setting up a CDN)

To address this issues, we propose to put the information of the "currently up-to-date release version(s)" in DNS records:

- by properly choosing its DNS resolver, the client can better protect its privacy
- DNS servers are optimized for speed and scaling
- DNS caching reduces the load on the authorative server

Software providers publish a TXT record under `_release.{full-qualified-domain}`, which can be queried by the software itself to check the currently up-to-date release version(s):

```
_release.example.org.	3600	IN	TXT	"v=1.2.3;answer=42;other_key=spaced=20value"
```

The goal is only to _inform_ the user if the software is not up to date. Updating the software itself is out of scope, since it depends on the distribution (package manager update, OCI image pull, binary download...).

See [draft-release-version-check-over-dns.md](./draft-release-version-check-over-dns.md) for the full specification (🚧 Work in progress).

## Known Implementations

- Go client: [code.pfad.fr/rvcodns](https://code.pfad.fr/rvcodns)
- Forgejo action to update OVH DNS record: [code.forgejo.org/actions/ovh-dns-update](https://code.forgejo.org/actions/ovh-dns-update)

## History

2022-12-22: idea shared by [@oliverpool](https://codeberg.org/oliverpool) in [#forgejo-chat:matrix.org](https://matrix.to/#/!qjPHwFPdxhpLkXMkyP:matrix.org/$hbwxRZY7y3PovymAcBvZ_euey3cv27JYuHFgEJ0nNfM?via=schinas.net&via=matrix.org&via=tchncs.de)

2023-01-27: pull request opened by [@Gusted](https://codeberg.org/Gusted) to add this feature to Forgejo: [forgejo/forgejo#278](https://codeberg.org/forgejo/forgejo/pulls/278)

2023-02-17: suggestion to standardize it by [@wicinski](https://codeberg.org/wicinski): [forgejo/forgejo#379](https://codeberg.org/forgejo/forgejo/issues/379)

2023-07-08: inclusion in the workplan of a [grant funded by NLNet for Forgejo](https://codeberg.org/forgejo/sustainability/src/branch/main/2022-12-01-nlnet/2023-06-workplan.md#dns-update-checker-rfc)
