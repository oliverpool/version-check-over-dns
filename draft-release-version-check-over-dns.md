%%%
title = "Release Version Check Over DNS"
abbrev = "Release Version Check Over DNS (RVCoDNS)"
docName = "draft-release-version-check-over-dns"
category = "info"

ipr = "trust200902"
area = "Internet"
# workgroup = "TODO Working Group"
# keyword = ["Internet-Draft"]

[pi]
toc = "yes"

[[author]]
initials = "O."
surname = "Olivier"
fullname = "Olivier Charvin"
organization = "Forgejo"
  [author.address]
   email = "olivier@pfad.fr"

%%%


.# Abstract

This document describes the use of DNS to inform software about the currently supported versions. It aims to replace version checking, which is usually done via HTTPS, which can be harmful to privacy and is often not scalable.

{mainmatter}

# Introduction

Normally, the current version of a software is checked by retrieving a version.json file via HTTP(S) and comparing its value. Performing such a check via HTTP(S) is not perfect:
- It reveals the IP address of the system running the software (could lead to privacy issues).
- It is difficult to scale (requires a CDN to be set up).

To solve this problem, we suggest including the information about the 'latest release version(s)' in special '_release' DNS resource records:
- By choosing their DNS resolver correctly, the user can better protect their privacy.
- DNS servers are optimised for speed and scaling.
- DNS caching reduces the load on the authoritative nameserver.

## Terminology

The key words "**MUST**", "**MUST NOT**", "**REQUIRED**", "**SHALL**", "**SHALL NOT**",
"**SHOULD**", "**SHOULD NOT**", "**RECOMMENDED**", "**NOT RECOMMENDED**", "**MAY**", and
"**OPTIONAL**" in this document are to be interpreted as described in BCP 14 [@!RFC2119] [@!RFC8174]
when, and only when, they appear in all capitals, as shown here.

## Out of Scope

Updating the software itself is out of scope, since it depends on the distribution mean (package manager update, OCI image pull, tarball or binary download for instance).

# Key Concepts

## DNS Resource Records

Domain Owner up-to-date Release Versions are stored as DNS TXT records in
subdomains named "_release" (underscored name, as recommended by [@RFC8552]).
or example, the Domain Owner of "example.org" would post Release Versions in a
TXT record at "_release.example.com".

Multiple records are allowed, to enable multiple versions to co-exist (for instance v1 and v2).
The software **MUST** be able to handle this case and **SHOULD NOT** inform the user if one of
the published version is the same as currently running.

## Syntax

// TODO

Domain Name System (DNS) concepts are specified in "Domain names - concepts and facilities" [@RFC1034].

DomainKeys Identified Mail (DKIM) Signatures [@RFC6376].

Applying Generate Random Extensions And Sustain Extensibility (GREASE) [@RFC8701]

# Background


TAKEN FROM https://datatracker.ietf.org/doc/html/rfc7489#section-6.1

Domain Owner DMARC preferences are stored as DNS TXT records in
subdomains named "_dmarc".  For example, the Domain Owner of
"example.com" would post DMARC preferences in a TXT record at
"_dmarc.example.com".  Similarly, a Mail Receiver wishing to query
for DMARC preferences regarding mail with an RFC5322.From domain of
"example.com" would issue a TXT query to the DNS for the subdomain of
"_dmarc.example.com".  The DNS-located DMARC preference data will
hereafter be called the "DMARC record".

DMARC's use of the Domain Name Service is driven by DMARC's use of
domain names and the nature of the query it performs.  The query
requirement matches with the DNS, for obtaining simple parametric
information.  It uses an established method of storing the
information, associated with the target domain name, namely an
isolated TXT record that is restricted to the DMARC context.  Use of
the DNS as the query service has the benefit of reusing an extremely
well-established operations, administration, and management
infrastructure, rather than creating a new one.

Per [DNS], a TXT record can comprise several "character-string"
objects.  Where this is the case, the module performing DMARC
evaluation MUST concatenate these strings by joining together the
objects in order and parsing the result as a single string.

TAKEN FROM https://datatracker.ietf.org/doc/html/rfc7489#section-6.3

DMARC records follow the extensible "tag-value" syntax for DNS-based
key records defined in DKIM [DKIM].

# Usability Considerations

## Displaying Release Information

When informing the adminstrator of a newer version, the message **SHOULD** take into account that software may often not be installed through a binary.
It is possible that the software is installed via an OCI image or a package manager.
If this is not controlled by the developers of the software, the other sources may not yet have been updated.
Similarly, it **MUST** be possible to disable version checking via a configuration option that can be enabled by a package manager.
Distributions are often not rolling releases and may deliberately want to delay the update to a new version, this should be supported by the software to not display new version announcements.

# IANA Considerations

_release should be added to the IANA "Underscored and Globally Scoped DNS Node Names" registry according to [@RFC8553] and [@RFC8126].

RR Type:    TXT
_NODE NAME: _release
Reference:  this document

# Security Considerations

## Client

When displayed to the user, the release version and other related information **MUST** be properly sanitized to prevent any security issue or misunderstanding.

If the domain used to retrieve the data supports DNSSEC [@RFC9364], the user **SHOULD** setup the system running the software to use a DNS resolver that supports and validates DNSSEC.

If applicable, the client **MUST** hardcode the link to a global download list from which the new software can be downloaded.

## Server

Updating DNS records is a sensitive operation. Proper right management **SHOULD** be used to ensure that only "_release" records can be updated during the (automated or manual) release process.

The domain name used to provide the DNS records must **SHOULD** support and have DNSSEC [@RFC9364] set up.

You **MUST NOT** distribute the download link for the new version to prevent potentially malicious links from being distributed.

{backmatter}
